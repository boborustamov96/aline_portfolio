from django.urls import path
from rest_framework.routers import DefaultRouter

from main.views import AboutReadOnlyViewSet, ProjectReadOnlyViewSet, TeamMemberReadOnlyViewSet, FeedbackAPIView

router = DefaultRouter()

router.register('about', AboutReadOnlyViewSet, basename='about')
router.register('projects', ProjectReadOnlyViewSet, basename='projects')
router.register('team_members', TeamMemberReadOnlyViewSet, basename='team_members')

urlpatterns = [path('feedback/', FeedbackAPIView.as_view())]

urlpatterns += router.urls
