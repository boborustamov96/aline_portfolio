from django.contrib import admin
from django.utils.html import format_html

from main.models import About, ProjectImage, TeamMember, Feedback, Contact, Project


# class ProductImageAdmin(admin.TabularInline):
#     model = ProductImage
#     extra = 1
#
#
# class ProductItemsAdmin(admin.ModelAdmin):
#     inlines = [ProductImageAdmin]
#     list_display = ('title', 'price', 'size_chart', 'size', 'quantity', 'color')
#     search_fields = ('title', 'price')
#     list_filter = ('title', 'price', 'size_chart', 'size', 'quantity',)
#     list_max_show_all = 500
#     list_per_page = 200


class ProjectImageAdmin(admin.TabularInline):
    model = ProjectImage
    extra = 3


class ProjectAdmin(admin.ModelAdmin):
    inlines = [ProjectImageAdmin, ]

    def image_tag(self, obj):
        if obj.logo:
            return format_html('<img src="{}" width="150" height="150"/ >'.format(obj.logo.url))
        else:
            return None

    image_tag.short_description = 'logo'

    list_display = ['image_tag', 'id', 'title', 'text', 'link', 'logo']


class TeamMemberAdmin(admin.ModelAdmin):

    def image_tag(self, obj):
        if obj.photo:
            return format_html('<img src="{}" width="150" height="150"/ >'.format(obj.photo.url))
        else:
            return None

    image_tag.short_description = 'photo'

    list_display = ['image_tag', 'id', 'first_name', 'last_name', 'photo', 'resume_link']


class ContactAdmin(admin.ModelAdmin):
    def image_tag(self, obj):
        if obj.contact_logo:
            return format_html('<img src="{}" width="150" height="150"/ >'.format(obj.contact_logo.url))
        else:
            return None

    image_tag.short_description = 'contact_logo'

    list_display = ['image_tag', 'id', 'name', 'link', 'contact_logo']


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'email', 'phone_number', 'question']


admin.site.register(About)
admin.site.register(Project, ProjectAdmin)
admin.site.register(TeamMember, TeamMemberAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Contact, ContactAdmin)
