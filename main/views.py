from rest_framework import viewsets
from rest_framework.generics import CreateAPIView

from main.models import About, Project, TeamMember, Feedback
from main.serializers import AboutSerializer, ProjectSerializer, TeamMemberSerializer, FeedbackSerializer


class AboutReadOnlyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = About.objects.all()
    serializer_class = AboutSerializer


class ProjectReadOnlyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class TeamMemberReadOnlyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TeamMember.objects.all()
    serializer_class = TeamMemberSerializer


class FeedbackAPIView(CreateAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
