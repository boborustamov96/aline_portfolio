from rest_framework import serializers

from main.models import About, ProjectImage, Project, TeamMember, Feedback


class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = About
        fields = ['id', 'title', 'text', 'company_image']


class ProjectImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImage
        fields = ['id', 'image']


class ProjectSerializer(serializers.ModelSerializer):
    project_image = ProjectImageSerializer(many=True)

    class Meta:
        model = Project
        fields = ['id', 'title', 'text', 'logo', 'link', 'project_image']


class TeamMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMember
        fields = ['id', 'first_name', 'last_name', 'description', 'photo', 'resume_link']


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ['id', 'first_name', 'last_name', 'email', 'phone_number', 'question']
