from django.db import models


class About(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True, verbose_name='Название Компании')
    text = models.TextField(null=True, blank=True, verbose_name='О нас')
    company_image = models.ImageField(
        null=True, blank=True,
        verbose_name='Картинка компании',
        upload_to='images/company_image'
    )

    class Meta:
        verbose_name = 'О нас'
        verbose_name_plural = 'О нас'

    def __str__(self):
        return self.text


class Project(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True, verbose_name='Заголовок проекта')
    text = models.TextField(null=True, blank=True, verbose_name='Текст проекта')
    logo = models.ImageField(null=True, blank=True, verbose_name='Лого проекта', upload_to='images/logo')
    link = models.URLField(null=True, blank=True, verbose_name='Ссылка на проект')

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return self.title


class ProjectImage(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='project_image')
    image = models.FileField(null=True, blank=True, upload_to='images/project_image')

    class Meta:
        verbose_name = 'Картинка Проекта'
        verbose_name_plural = 'Картинки Проекта'


class TeamMember(models.Model):
    first_name = models.CharField(max_length=255, null=True, blank=True, verbose_name='Имя')
    last_name = models.CharField(max_length=255, null=True, blank=True, verbose_name='Фамилия')
    photo = models.FileField(null=True, blank=True, upload_to='images/team_member_photo',
                             verbose_name='Фото сотрудника')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    resume_link = models.URLField(null=True, blank=True, verbose_name='Ссылка на резюме')

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return f'{self.first_name} -- {self.last_name}'


class Feedback(models.Model):
    first_name = models.CharField(max_length=255, verbose_name='Имя')
    last_name = models.CharField(max_length=255, verbose_name='Фамилия')
    email = models.EmailField(verbose_name='Почта')
    phone_number = models.CharField(max_length=255, verbose_name='Номер Телефона')
    question = models.TextField(verbose_name='Вопрос')

    class Meta:
        verbose_name = 'Обратная Связь'
        verbose_name_plural = 'Обратная Связь'

    def __str__(self):
        return self.email


class Contact(models.Model):
    contact_logo = models.FileField(null=True, blank=True, upload_to='images/contact_logo', verbose_name='Лого сервиса')
    link = models.URLField(null=True, blank=True, verbose_name='Ссылка на сервис')
    name = models.CharField(max_length=255, null=True, blank=True, verbose_name='Название сервиса')

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return self.name
